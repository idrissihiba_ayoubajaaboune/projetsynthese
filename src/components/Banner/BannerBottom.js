import React from "react";
import './index1.css';
import serviceImage1 from "../../assets/images/banner/serviceImage1.jpg";
import serviceImage2 from "../../assets/images/banner/serviceImage2.jpg";
import serviceImage3 from "../../assets/images/banner/serviceImage3.jpeg";
import serviceImage4 from "../../assets/images/banner/serviceImage4.jpeg";

const BannerBottom = () => {
  return (
    <section className="container">
      <div className="text">
        <h2>We have the best services available for you!</h2>
      </div>
      <div className="rowitems">
        <div className="container-box">
          <div className="container-image">
            <a href='/about'><img src={serviceImage1} alt="Restoration Services" /></a>
          </div>
          <h4>Restoration</h4>
          <p>Virtual Restoration Workshops</p>
        </div>

        <div className="container-box">
          <div className="container-image">
          <a href='/about'><img src={serviceImage2} alt="Painting" /></a>
          </div>
          <h4>Painting</h4>
          <p>Online Painting Classes</p>
        </div>

        <div className="container-box">
          <div className="container-image">
          <a href='/about'><img src={serviceImage3} alt="Tours Services" /></a>
          </div>
          <h4>Tours</h4>
          <p>Virtual Gallery Tours(Online)</p>
        </div>

        <div className="container-box">
          <div className="container-image">
          <a href='/about'><img src={serviceImage4} alt="Appraisal Services" /></a>
          </div>
          <h4>Appraisal</h4>
          <p>Artwork Assessment Tools</p>
        </div>
      </div>
    </section>
  );
}


export default BannerBottom;
