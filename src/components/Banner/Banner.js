import React from 'react';
import './index.css';
import vedio from '../../assets/images/banner/vidmu.mp4'
const Interface = () => {
  return (
    <div className="banner">
          <video src={vedio} type="video/mp4" autoPlay muted loop></video>

      <div className="content" id="home">
        <div className="title">
          <h1>ART MUSEUM</h1>
          <p>Where history speaks, and art whispers."</p>
          <a href="./register" className="button">Register Now!</a>
        </div>
      </div>
    </div>
  );
}

export default Interface;

