import React from "react";
import Product from "../Products/Product";
import {
  s3,
  s4,
  s2,
  s5,
} from "../../../assets/images/index";

const NewArrivals = () => {
  return (
    <div className="w-full pb-20">
      <div className="flex items-center mb-5">
        <span className='border border-t-0 border-r-0 border-b-0 border-[rgb(125,41,43)] py-7 bg-[rgb(125,41,43)] mr-5'></span>
        <h1 className='text-[rgb(125,41,43)] text-lg font-semibold mr-5'>Our Bestsellers</h1>
      </div>
      <div className="flex justify-center mb-5">
        <h1 style={{ color: "white", fontSize: "2rem", textAlign: "center" }}>Scluptures</h1>
      </div>
      <div className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-10">
        <Product
          _id="1011"
          img={s5}
          productName="Mona Lisa (La Joconde)"
          price="499dh"
          color="January 31,1867"
          badge={true}
          des="Lisa (15 juin 1479, Florence - 15 juillet 1542), aussi connue sous le nom de Mona Lisa, Lisa di Antonio Maria (Antonmaria) Gherardini et de Lisa del Giocondo en italien, est un membre de la famille Gherardini de Florence. Elle serait le modèle de La Joconde, portrait commandé par son mari et peint par Léonard de Vinci."
        />
        <Product
          _id="1012"
          img={s3}
          productName="Van Gogh"
          price="180DH"
          color="January 31,1867"
          badge={false}
          des="Vincent Willem van Gogh was a Dutch Post-Impressionist painter who is among the most famous and influential figures in the history of Western art. In just over a decade he created approximately 2100 artworks, including around 860 oil paintings, most of them in the last two years of his life."
        />
        <Product
          _id="1013"
          img={s4}
          productName="Aphrodite"
          price="299DH"
          color="January 31,1867"
          badge={true}
          des="Aphrodite is an ancient Greek goddess associated with love, lust, beauty, pleasure, passion, procreation, and as her syncretized Roman goddess counterpart Venus, desire, sex, fertility, prosperity, and victory. Aphrodite's major symbols include seashells, myrtles, roses, doves, sparrows, and swans. The cult of Aphrodite was largely derived from that of the Phoenician goddess Astarte, a cognate of the East Semitic goddess Ishtar, whose cult was based on the Sumerian cult of Inanna."
        />
        <Product
          _id="1014"
          img={s2}
          productName="The Unequal Marriage"
          price="220.00"
          color="January 31,1867"
          badge={false}
          des="The Unequal Marriage by Vasili Pukirev appears at the far right of the canvas (possibly as best man), giving rise to the story that it represented an episode of lost love in his own life.[2] In 1863, on the basis of this work, he was named an honorary Professor at the Imperial Academy of Fine Arts."
        />
      </div>
    </div>
  );
};

export default NewArrivals;
