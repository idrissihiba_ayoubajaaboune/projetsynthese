import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FaBars, FaTimes, FaShoppingCart } from "react-icons/fa"; // Import FaShoppingCart icon
import './index.css';

const Header = () => {
 const navBarList = [
    {
      _id: 1001,
      title: "Home",
      link: "/",
    },
    {
      _id: 1003,
      title: "About Us",
      link: "/about",
    },
    {
      _id: 1005,
      title: "Sculptures",
      link: "/journal",
    },
    {
      _id: 1006,
      title: "Tableaux",
      link: "/journal",
    },
    {
      _id: 1002,
      title: "Shop",
      link: "/shop",
    },
 
    {
      _id: 1004,
      title: "Contact Us",
      link: "contact",
    },

  ];

  const [scrolling, setScrolling] = useState(false);
  const [menu, setMenu] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setScrolling(true);
      } else {
        setScrolling(false);
      }
    };
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleMenu = () => setMenu(!menu);

  const handleCartClick = () => {
    console.log("Cart clicked!");
  };

  return (
    <div className={scrolling ? 'header scrolled' : 'header'}>
      <Link to="/">
        <a href="#" className="logo">ART MUSEUM</a>
      </Link>
      <ul className={menu ? "nav-menu active" : "nav-menu"}>
        {navBarList.map((navBarItem, index) => (
          <li key={index}>
            <Link to={navBarItem.link}>{navBarItem.title}</Link>
          </li>
        ))}
      </ul>
      <div className='cart' onClick={handleCartClick}>
        <Link to="/cart">
          <FaShoppingCart size={25} style={{ color: "black" }} />
        </Link>
      </div>
      <div className="humberger" onClick={handleMenu}>
        {menu ? (<FaTimes size={20} style={{ color: "beige" }} />) : (<FaBars size={20} style={{ color: "beige" }} />)}
      </div>
    </div>
  )
}

export default Header;
