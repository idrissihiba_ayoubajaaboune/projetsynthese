import React from 'react';
import './index.css'; 
import { Link } from 'react-router-dom';


const Footer = () => {
  return (
    <footer className="footer">
      <div className="foot">
        <div className="footer-content">
          
          <div className="footlinks">
            <h4>Quick Links</h4>
            <ul>
            <li>
              <Link to="/">Home</Link>
              <Link to="./about">About Us</Link>
              <Link to="./tableaux">Tableaux</Link>
              <Link to="./scluptures">Scluptures</Link>
              <Link to="./register">Register</Link>
              <Link to="./contact">Contact Us</Link>
            </li>
            </ul>
          </div>

          <div className="footlinks">
            <h4>Connect</h4>
            <div className="social">
              <a href="https://www.facebook.com/Art_Museum" target="_blank"><i className='bx bxl-facebook'></i></a>
              <a href="https://www.instagram.com/Art_Museum" target="_blank"><i className='bx bxl-instagram'></i></a>
              <a href="https://www.twitter.com/Art_Museum1" target="_blank"><i className='bx bxl-twitter'></i></a>
              <a href="https://www.linkedin.com/in/Art_Museum" target="_blank"><i className='bx bxl-linkedin'></i></a>
              <a href="https://www.youtube.com/Art_Museum" target="_blank"><i className='bx bxl-youtube'></i></a>
              <a href="https://www.github.com/Art_Museum" target="_blank"><i className='bx bxl-github'></i></a>
            </div>
          </div>
          
        </div>
      </div>

      <div className="end">
        <p>Copyright © 2024 Art Museum All Rights Reserved.<br />Website developed by: A & H</p>
      </div>
    </footer>
  );
}

export default Footer;
