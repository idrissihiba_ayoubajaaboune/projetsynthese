import React from "react";
import Banner from "../../components/Banner/Banner";
import BannerBottom from "../../components/Banner/BannerBottom";
import NewArrivals from "../../components/home/NewArrivals/NewArrivals";
import NewLetter from "../../components/home/NewLetter/NewLetter";
import BestSellers from "../../components/home/BestSellers/BestSellers";

const Home = () => {
  return (
    <div className="w-full mx-auto">
      <Banner/>
      <BannerBottom />
      <div className="max-w-container mx-auto px-3">
        <NewArrivals/>
        <BestSellers/>
        <NewLetter/>
      </div>
    </div>
  );
};

export default Home;
