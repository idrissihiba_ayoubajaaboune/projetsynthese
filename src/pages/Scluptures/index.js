import React from 'react';
import './index.css'; // Assuming this is where your CSS styles are defined

import scluptures from './sculpturesData'

const Scluptures = () => {
    
    return (
        <>
            <section className="Sculptures" id="Sculptures">
                <div className="package-title">
                    <h2>Sculptures</h2>
                </div>

                <div className="Sculpture-content" style={{ display: 'flex', justifyContent: 'space-around', flexWrap: 'wrap' }}>
                    {scluptures.map((sculpture) => (
                        <a href={`./Scluptures/${sculpture.title}`} target="_blank" key={sculpture.id}>
                            <div className="col-content" style={{ position: 'relative', filter: 'brightness(95%)', transition: 'all 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95)', ':hover': { filter: 'brightness(120%)', transform: 'scale(1.03)' } }}>
                                <img src={sculpture.image} alt="" style={{ marginBottom: '40px', height: '400px', borderRadius: '27px', width: '150%' }} />
                                <h5 style={{ position: 'absolute', fontSize: '25px', color: 'white', fontWeight: '500', left: '20px', bottom: '105px', cursor: 'pointer' }}>{sculpture.title}</h5>
                                <p style={{ position: 'absolute', fontSize: '20px', color: 'white', left: '20px', bottom: '80px', cursor: 'pointer' }}>{sculpture.category}</p>
                                <p style={{ position: 'absolute', fontSize: '20px', color: 'white', left: '20px', bottom: '55px', cursor: 'pointer' }}>Price: ${sculpture.price}</p>
                            </div>
                        </a>
                    ))}
                </div>
            </section>
        </>
    );
}

export default Scluptures;
