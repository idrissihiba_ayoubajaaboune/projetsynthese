import pic1 from '../../images/s1.jpg'
import pic2 from '../../images/s2.jpg'
import pic3 from '../../images/s3.jpg'
import pic4 from '../../images/s4.jpg'
import pic5 from '../../images/s5.jpg'
import pic6 from '../../images/s6.jpg'
import pic7 from '../../images/s7.jpg'
import pic8 from '../../images/s9.jpg'
import pic9 from '../../images/s10.jpg'
import pic10 from '../../images/s12.jpg'


const scluptures = [
    {
        "id": 12,
        "title": "Alexander",
        "price": 56.99,
        "description": "Alexander (Greek: Ἀλέξανδρος) is a male given name of Greek origin. The most prominent bearer of the name is Alexander the Great, the king of the Ancient Greek kingdom of Macedonia who created one of the largest empires in ancient history",
        "category": "Old Sculptures",
        "image": pic1,
        "rating": {
            "rate": 2.6,
            "count": 235
        }
    },
    {
        
        "id": 12,
        "title": "Zues",
        "price": 699.67,
        "description": "Zeus is the sky and thunder god in ancient Greek religion and mythology, who rules as king of the gods on Mount Olympus. His name is cognate with the first syllable of his Roman equivalent Jupiter",
        "category": "Old Sculptures",
        "image": pic2,
        "rating": {
            "rate": 5.9,
            "count": 265
        }
    },
    {
        "id": 13,
        "title": "Lucius Aurelius Verus",
        "price": 599.89,
        "description": "Lucius Aurelius Verus (15 December 130 – January/February 169) was Roman emperor from 161 until his death in 169, alongside his adoptive brother Marcus Aurelius. He was a member of the Nerva–Antonine dynasty. Verus' succession together with Marcus Aurelius marked the first time that the Roman Empire was ruled by more than one emperor simultaneously, an increasingly common occurrence in the later history of the Empire..",
        "category": "Old Sculptures",
        "image": pic3,
        "rating": {
            "rate": 8.9,
            "count": 367
        }
    },
    {
        "id": 14,
        "title": "Drive",
        "price": 490.78,
        "description": "Expand your PS4 gaming experience, Play anywhere Fast and easy, setup Sleek design with high capacity, 3-year manufacturer's limited warranty",
        "category": "Old Sculptures",
        "image": pic4,
        "rating": {
            "rate": 6.8,
            "count": 100
        }
    },
    {
        "id": 15,
        "title": "Jean-Isaac de Thellusson de Sorcy",
        "price": 599.09,
        "description": "Bust of the Countess Jean-Isaac de Thellusson de Sorcy is a painting by Jean-Antoine Houdon which was uploaded on May 25th, 2021",
        "category": "Old Sculptures",
        "image": pic5,
        "rating": {
            "rate": 9.1,
            "count": 220
        }
    },
    {
        "id": 16,
        "title": "David is a masterpiece",
        "price": 799.99,
        "description": "David is a masterpiece of Italian Renaissance sculpture, created from 1501 to 1504 by Michelangelo. With a height of 5.17 metres (17 ft 0 in), the David was the first colossal marble statue made in the early modern period following classical antiquity, a precedent for the 16th century and beyond. David was originally commissioned as one of a series of statues of twelve prophets to be positioned along the roofline of the east end of Florence Cathedral (Duomo di Firenze)",
        "category": "Old Sculptures",
        "image": pic6,
        "rating": {
            "rate": 7.2,
            "count": 599
        }
    },
    {
        "id": 17,
        "title": "Alexander",
        "price": 56.99,
        "description": "Alexander (Greek: Ἀλέξανδρος) is a male given name of Greek origin. The most prominent bearer of the name is Alexander the Great, the king of the Ancient Greek kingdom of Macedonia who created one of the largest empires in ancient history",
        "category": "Old Sculptures",
        "image": pic7,
        "rating": {
            "rate": 2.6,
            "count": 235
        }
    },
    {
        "id": 18,
        "title": "Aphrodite",
        "price": 699.95,
        "description": "Aphrodite is an ancient Greek goddess associated with love, lust, beauty, pleasure, passion, procreation, and as her syncretized Roman goddess counterpart Venus, desire, sex, fertility, prosperity, and victory. Aphrodite's major symbols include seashells, myrtles, roses, doves, sparrows, and swans. The cult of Aphrodite was largely derived from that of the Phoenician goddess Astarte, a cognate of the East Semitic goddess Ishtar, whose cult was based on the Sumerian cult of Inanna.",
        "category": "Old Sculptures",
        "image": pic8,
        "rating": {
            "rate": 7.9,
            "count": 320
        }
    },
    {
        "id": 19,
        "title": " Bust Of a Woman (antonio tantardini)",
        "price": 387.95,
        "description": "This beautiful bust shows a contemplative young woman with an elaborate up-do and finely rendered jewellery. It epitomises the virtuoso carving technique of Antonio Tantardini who, in a country famed for its skilled marble carvers, was one of the very best exponents of this craft.",
        "category": "Old Scluptures",
        "image": pic9,
        "rating": {
            "rate": 6.5,
            "count": 147
        }
    },
    {
        "id": 20,
        "title": "Madame de Wailly",
        "price": 400.99,
        "description": "The sitter was the wife of Pajou's lifelong friend Charles de Wailly, a companion from student days in Rome. De Wailly, court architect to Louis XVI, had built neighboring houses for Pajou and himself, and Pajou executed busts of the architect and his wife. After her husband's death, Madame de Wailly married M. de Fourcroy, a medical doctor and chemist.",
        "category": "Old Scluptures",
        "image": pic10,
        "rating": {
            "rate": 6.6,
            "count": 140
        }
    }
];

export default scluptures;