// import React, { useState } from 'react';
// import './index.css'; 

// const SignUp = () => {
//   const [username, setUsername] = useState('');
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');
//   const [confirmPassword, setConfirmPassword] = useState('');

//   const handleUsernameChange = (event) => {
//     setUsername(event.target.value);
//   };

//   const handleEmailChange = (event) => {
//     setEmail(event.target.value);
//   };

//   const handlePasswordChange = (event) => {
//     setPassword(event.target.value);
//   };

//   const handleConfirmPasswordChange = (event) => {
//     setConfirmPassword(event.target.value);
//   };

//   const handleSubmit = (event) => {
//     event.preventDefault();
//     // Add your registration logic here
//     console.log('Username:', username);
//     console.log('Email:', email);
//     console.log('Password:', password);
//     console.log('Confirm Password:', confirmPassword);
//   };

//   return (
//     <div className='bd'>
//       <div className="wrapper">
//         <form onSubmit={handleSubmit}>
//           <h1>Register</h1>
//           <div className="input-box">
//             <input
//               type="text"
//               placeholder="Username"
//               value={username}
//               onChange={handleUsernameChange}
//               required
//             />
//             <i className='bx bxs-user'></i>
//           </div>
//           <div className="input-box">
//             <input
//               type="email"
//               placeholder="Email"
//               value={email}
//               onChange={handleEmailChange}
//               required
//             />
//             <i className='bx bxs-envelope'></i>
//           </div>
//           <div className="input-box">
//             <input
//               type="password"
//               placeholder="Password"
//               value={password}
//               onChange={handlePasswordChange}
//               required
//             />
//             <i className='bx bxs-lock-alt'></i>
//           </div>
//           <div className="input-box">
//             <input
//               type="password"
//               placeholder="Confirm Password"
//               value={confirmPassword}
//               onChange={handleConfirmPasswordChange}
//               required
//             />
//             <i className='bx bxs-lock-alt'></i>
//           </div>
//           <button type="submit" className="btn">Register</button>
//           <div className="register-link">
//             <p>Already have an account? <a href="/contact">Login</a></p>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default SignUp;
